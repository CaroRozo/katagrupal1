import math

class Solver:

     def calcularNumeroElementos(self, cadena):
         if cadena == "":
            return [0,0,0,0]
         elif "," not in cadena:
            return [1,int(cadena),int(cadena),int(cadena)]
         elif "," in cadena:
             numeros = cadena.split(",")
             promedio = 0.0
             minimo = int(numeros[1])
             maximo = int(numeros[1])
             for num in numeros:
                if int(num) < int(minimo):
                    minimo = num

                if int(num) > int(maximo):
                    maximo = num
                promedio= promedio+ int(num)
             promedio /= len(numeros)

             return [len(numeros),int(minimo), int(maximo),promedio]
         else:
            return -1