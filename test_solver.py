from unittest import TestCase
from solver import Solver


class TestSolver(TestCase):
    def test_calcularNumeroElementos(self):
        self.assertEquals(Solver().calcularNumeroElementos(""),[0,0,0,0],"Cero elementos")

    def test_calcularNumeroElementosUnElemento(self):
        self.assertEquals(Solver().calcularNumeroElementos("1"),[1,1,1,1],"Un elemento")

    def test_calcularNumeroElementosDosElementos(self):
        self.assertEquals(Solver().calcularNumeroElementos("2,1"),[2,1,2,1.5],"Dos elemento")

    def test_calcularNumeroElementosVariosElementos(self):
         self.assertEquals(Solver().calcularNumeroElementos("2,1,3,4,5"),[5,1,5,3],"Varios elementos")